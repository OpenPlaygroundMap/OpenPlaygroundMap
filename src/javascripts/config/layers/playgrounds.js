/**
* @license AGPL-3.0
* @author aAXEe (https://github.com/aAXEe)
*/
'use strict'

import React from 'react'
import { ClickOnMarkersMessage } from 'utils'
import {TabSidebarDetails} from 'features/tabs'
import controlIds from '../../controls/ol3/controls'
import orderIds from '../layerOrderNumbers'

import ol from 'openlayers'
import ChartLayer from '../chartlayer'
import Colors from '../../../../colors.json'
import tinycolor2 from 'tinycolor2'

import OverpassApi from 'ol-source-overpassApi'

import { featureClicked, layerTileLoadStateChange } from '../../store/actions'
import { setSidebarOpen, setSidebarActiveTab } from '../../controls/sidebar/store'

import { defineMessages } from 'react-intl'
export const messages = defineMessages({
  layerName: {
    id: 'layer-name-playgrounds',
    defaultMessage: 'Interactive playgrounds'
  }
})

const FEATURE_CLICKED_PROPERTY_NAME = '_clicked'
const FEATURE_HOVERED_PROPERTY_NAME = '_hovered'

module.exports = function (context, options) {
  var defaults = {
    nameKey: 'layer-name-playgrounds'
  }
  Object.assign(defaults, options)

  const labelCircleFilleColor = tinycolor2(Colors['mid-green'])
  labelCircleFilleColor.setAlpha(0.5)

  const labelGrid = ol.tilegrid.createXYZ({maxZoom: 22})
  var styleFunction = function (feature, resolution) {
    const z = labelGrid.getZForResolution(resolution)
    let clicked = feature.get(FEATURE_CLICKED_PROPERTY_NAME)
    let hovered = feature.get(FEATURE_HOVERED_PROPERTY_NAME)

    const image = new ol.style.Circle({
      radius: 10,
      fill: new ol.style.Fill({
        color: labelCircleFilleColor.toRgbString()
      }),
      stroke: new ol.style.Stroke({
        color: Colors['dark-green'],
        width: hovered || clicked ? 3 : 1
      })
    })
    const style = new ol.style.Style({
      image: image
    })

    let showLabel = clicked || hovered

    if (z >= 14) showLabel = true

    if (showLabel) {
      const name = feature.get('name')
      const nameElement = new ol.style.Text({
        font: hovered ? 'bold 12px sans-serif' : '10px sans-serif',
        offsetY: 12,
        text: name,
        textAlign: 'center',
        textBaseline: 'top'
      })
      style.setText(nameElement)
    }

    return style
  }
/*
  let sourceLabels = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: '/hamburg_germany.playgrounds.geojson'
  }) */
  let overpassSource = new OverpassApi('(node["leisure"="playground"](bbox););out body qt;')
  const source = overpassSource
  source.on(['tileloadstart', 'tileloadend', 'tileloaderror'], function (ev) {
    context.dispatch(layerTileLoadStateChange(options.id, ev))
  })

  let layer = new ol.layer.Vector({
    source: source,
    style: function (feature, resolution) {
      return styleFunction(feature, resolution, 'normal')
    },
    maxResolution: labelGrid.getResolution(12),
    zIndex: orderIds.user_overlay
  })

  layer.on('selectFeature', function (e) {
    let feature = e.feature
    feature.set(FEATURE_CLICKED_PROPERTY_NAME, true)
    context.dispatch(featureClicked(feature.getProperties()))
    context.dispatch(setSidebarActiveTab(TabSidebarDetails.name))
    context.dispatch(setSidebarOpen(true))
  })
  layer.on('unselectFeature', function (e) {
    e.feature.set(FEATURE_CLICKED_PROPERTY_NAME, false)
  })

  layer.on('hoverFeature', function (e) {
    let feature = e.feature
    feature.set(FEATURE_HOVERED_PROPERTY_NAME, true)
  })
  layer.on('unhoverFeature', function (e) {
    e.feature.set(FEATURE_HOVERED_PROPERTY_NAME, false)
  })

  const clusterSource = new ol.source.Cluster({
    distance: 50,
    source: source
  })

  const styleCache = {}
  var clusters = new ol.layer.Vector({
    source: clusterSource,
    minResolution: labelGrid.getResolution(12),
    renderOrder: (f1, f2) => {
      return f1.get('features').length - f2.get('features').length
    },
    zIndex: orderIds.user_overlay,
    style: function (feature) {
      const size = feature.get('features').length
      let style = styleCache[size]
      if (!style) {
        const circleRadius = 10 + Math.log10(size) * 2
        style = new ol.style.Style({
          image: new ol.style.Circle({
            radius: circleRadius,
            stroke: new ol.style.Stroke({
              color: Colors['dark-green']
            }),
            fill: new ol.style.Fill({
              color: Colors['mid-green']
            })
          }),
          text: new ol.style.Text({
            text: size.toString(),
            fill: new ol.style.Fill({
              color: '#fff'
            }),
            textAlign: 'center',
            textBaseline: 'middle',
            font: 'bold 10px sans-serif'
          })
        })
        styleCache[size] = style
      }
      return style
    }
  })

  var objects = {
    layer: new ol.layer.Group({
      layers: [
        layer, clusters
      ]
    }),
    interactiveLayers: [layer],
    additionalSetup: (
      <div>
        <ClickOnMarkersMessage />
      </div>
    ),
    additionalTab: TabSidebarDetails,
    additionalControls: [controlIds.attribution]
  }

  return new ChartLayer(context, Object.assign(defaults, objects))
}
