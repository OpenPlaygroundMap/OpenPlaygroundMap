/**
* @license AGPL-3.0
* @author aAXEe (https://github.com/aAXEe)
* @author mojoaxel (https://github.com/mojoaxel)
*/
import _ from 'lodash'

import OsmBase from './layers/openStreetMapBase'
import BasemapVector from './layers/basemapVector'
import Search from './layers/search'
import BingBase from './layers/bingBase'
import DebugTiles from './layers/tilesDebug'
import Playgrounds from './layers/playgrounds'

export const availibleBaseLayers = [
  {
    LayerConstructor: OsmBase,
    id: 'base_osm_default',
    isBaseLayer: true,
    urlIndex2013: 1,
    urlIndex2016BaseLayer: 'A',
    visibleDefault: true
  },
  {
    LayerConstructor: BasemapVector,
    id: 'base_vector',
    isBaseLayer: true,
    urlIndex2016BaseLayer: 'B',
    visibleDefault: false
  },
  {
    LayerConstructor: BingBase,
    id: 'base_bing',
    isBaseLayer: true,
    urlIndex2013: 12,
    urlIndex2016BaseLayer: 'C',
    visibleDefault: false
  }
]

export const availibleOverlayLayers = [
  {
    LayerConstructor: Search,
    id: 'overlay_search',
    urlIndex2016: 2,
    visibleDefault: true
  },
  {
    LayerConstructor: DebugTiles,
    id: 'overlay_debug',
    urlIndex2016: 3,
    visibleDefault: false
  },
  {
    LayerConstructor: Playgrounds,
    id: 'overlay_playgrounds',
    urlIndex2016: 1,
    visibleDefault: true
  }
]

export function createLayers (store) {
  let layers = []
  let availibleLayers = availibleBaseLayers.concat(availibleOverlayLayers)
  availibleLayers.forEach(function (layer) {
    layers.push(new layer.LayerConstructor(store, _.omit(layer, 'LayerConstructor')))
  })
  return layers
}
