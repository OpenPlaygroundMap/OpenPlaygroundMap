
export const defaultViewPosition = {
  position: {
    lon: 11.435,
    lat: 53.61,
    zoom: 15
  }
}

import controlIds from './controls/ol3/controls'
export const alwaysOnControls = [
  controlIds.zoom, controlIds.fullscreen
]
