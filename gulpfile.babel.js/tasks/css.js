var config = require('../config')

var gulp = require('gulp')
var browserSync = require('browser-sync')
var sass = require('gulp-sass')
var sourcemaps = require('gulp-sourcemaps')
var handleErrors = require('../lib/handleErrors')
var autoprefixer = require('gulp-autoprefixer')
var path = require('path')
var concatCss = require('gulp-concat-css')
var jsonImporter = require('node-sass-json-importer')

var paths = {
  src: path.join(config.root.src, config.tasks.css.src, '/**/*.{' + config.tasks.css.extensions + '}'),
  dest: path.join(config.root.dest, config.tasks.css.dest)
}

var cssTask = function () {
  return gulp.src(paths.src)
    .pipe(sourcemaps.init())
    .pipe(sass(Object.assign(config.tasks.css.sass, {importer: jsonImporter})))
    .pipe(concatCss('global.css', config.tasks.css.cssimport))
    .on('error', handleErrors)
    .pipe(autoprefixer(config.tasks.css.autoprefixer))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream())
}

gulp.task('css', cssTask)
module.exports = cssTask
